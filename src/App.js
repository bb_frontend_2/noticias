import React,{Fragment, useState} from 'react';
import Header from './componentes/Header';
import Formulario from './componentes/Formulario';

function App() {

  return (
    <div className="App">
      <Header
        titulo = "Buscador de noticias"
      />
      <div className="container white ">
        <Formulario/>
      </div>
    </div>
  );
}

export default App;
