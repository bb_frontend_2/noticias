import React,{Fragment} from 'react';
import styled from '@emotion/styled';


function Header({titulo}) {
    return (
        <Fragment>
            <nav className="nav-wrapper light-blue darken-3">
                <a href= "#!" className="brand-logo center">{titulo}</a>
            </nav>
        </Fragment>
    );
}

export default Header;