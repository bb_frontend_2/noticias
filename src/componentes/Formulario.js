import React,{useState,useEffect} from 'react';
import styled from '@emotion/styled';
import axios from 'axios'
const FormularioStyle = styled.form`
    width:70%;
    height: auto;
    margin: 0 auto;
    display:flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin-top: 1rem;
`;
const SelectStyle = styled.select`
    width: 100%;
    height: 60px;
    line-height: 60px;
    background-color: #FFD486;
    border-radius: 5px;
    display: block;
    font-size: 22px;

`
const BotonConsultar = styled.input`
    width: 30%;
    height: 50px;
    border-radius: 5px;
    margin:1rem;
    background-color: #5AB1CF;
    border: none;
    font-size: 22px;
    &:hover{
        background-color: #2999A5;
        cursor: pointer;
    }
`
const DivNoticia = styled.div`
    width: 100%;
    display:flex;
    justify-content: center;
    align-items:start;
    flex-wrap: wrap;
    background-color: #CFCFDB;


`
const Noticia = styled.div`
    width: 30%;
    padding: 0.5rem;
    background-color: #0277BD;
    color: #fff;
    margin: 1rem;
    flex-wrap: flex-wrap;
`
const ParrafoError= styled.p`
    width:100%;
    height:50px;
    line-height:50px;
    background-color: red;
    color:#fff;
    font-weight: bold;
    text-align: center;
    font-size: 20px;
`
function Formulario() {
    const [paises, guardarPais] = useState([]);
    const  [state, actualizarState] = useState('');
    const [noticias, guardarNoticias]= useState([]);
    const [error, guardarError] = useState(false);


        const consumirApi = async()=>{
            const fecha = new Date();
            const pais= 'sa';
            console.log(fecha);
            const key = '4ac134c8d5128d2c86045a049d93dacb';
            const url = `http://api.mediastack.com/v1/news?access_key=${key}&countries=${state}`;
            const res = await axios.get(url);
            guardarNoticias(res.data.data);
            console.log(noticias);
        }

    const generarNoticia =(e)=>{
        e.preventDefault();
        consumirApi();
        if(noticias != "undefined" && noticias != null && noticias.length > 0){

            guardarError(true);
            return;
        }
        else{
            guardarError(false);
        }



    }
    console.log(state);
    useEffect(()=>{
        const consultarApi = async()=>{
            const url = 'https://restcountries.eu/rest/v2';
            const respuesta = await axios.get(url);
            const acceder = respuesta.data;
            guardarPais(acceder);
        }
        consultarApi();
    }, [])

    return (
        <div>
            <FormularioStyle
                onSubmit={generarNoticia}
            >
            <SelectStyle
                onChange={(e)=> actualizarState(e.target.value)}
                value = {state}>
             {
                 paises.map((pais)=>(
                     <option key = {pais.alpha2Code} value={pais.alpha2Code}>{pais.name}</option>
                     ))
                    }
            </SelectStyle>

            <BotonConsultar
                type="submit"
                value="Consultar"
                />

            </FormularioStyle>
                {
                    (error)?(<ParrafoError>No Hay noticias de este Pais Porfavor selecciona otro</ParrafoError>):null
                }
            <DivNoticia>
             {
                 noticias.map((noticia)=>(
                    <Noticia>
                        <h5>{noticia.title}</h5>
                        <span>{noticia.category}</span>
                        <p>{noticia.description}</p>
                    </Noticia>
                ))
             }
            </DivNoticia>

        </div>
    );
}

export default Formulario;